@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Student Registration</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(isset($record_to_edit) && !empty($record_to_edit))
                    <form method="POST" action="{{url('update-student/'.$record_to_edit[0]['id'].'')}}" id="update-form">
                    @else
                    <form method="POST" action="{{url('create-student')}}" id="create-form">
                    @endif
                        {!! csrf_field() !!}
                    <div class="row">
                      <div class="form-group col-md-3">
                           <label for="usr">First Name</label>
                           @if(isset($record_to_edit) && !empty($record_to_edit))
                           <input type="text" class="form-control" id="first_name" name="first_name" required="true" value="{{$record_to_edit[0]['first_name']}}">
                           @else
                           <input type="text" class="form-control" id="first_name" name="first_name" required="true">
                           @endif
                      </div>
                      <div class="form-group col-md-3">
                           <label for="usr">Last Name</label>
                           @if(isset($record_to_edit) && !empty($record_to_edit))
                           <input type="text" class="form-control" id="last_name" name="last_name" required="true" value="{{$record_to_edit[0]['last_name']}}">
                           @else
                           <input type="text" class="form-control" id="last_name" name="last_name" required="true">
                           @endif
                      </div>
                      <div class="form-group col-md-2">
                          <label for="usr">Teacher's Name</label>
                          <select class="form-control" id="teacher" name="teacher">
                            @foreach($teachers as $key => $teacher)
                            @if(isset($record_to_edit) && !empty($record_to_edit) && $record_to_edit[0]['teacher']['id'] == $teacher['id'])
                            <option value="{{$teacher['id']}}" selected="true">{{$teacher['name']}}</option>
                            @else
                            <option value="{{$teacher['id']}}">{{$teacher['name']}}</option>
                            @endif
                            @endforeach    
                          </select>
                      </div>
                      <div class="form-group col-md-2">
                          <label for="usr">Class</label>
                          <select class="form-control" id="class" name="class">
                            @foreach($classes as $key => $class)
                            @if(isset($record_to_edit) && !empty($record_to_edit) && $record_to_edit[0]['class']['id'] == $class['id'])
                            <option value="{{$class['id']}}" selected="true">{{$class['class']}}</option>
                            @else
                            <option value="{{$class['id']}}">{{$class['class']}}</option>
                            @endif
                            @endforeach
                          </select>
                      </div>
                      <div class="form-group col-md-2">
                          <label for="usr">Joined Year</label>
                          @if(isset($record_to_edit) && !empty($record_to_edit))
                          <input type="number" min="1900" max="2099" step="1" value="{{$record_to_edit[0]['joined_year']}}" class="form-control" id="joined_year" name="joined_year"/>
                          @else
                          <input type="number" min="1900" max="2099" step="1" value="2016" class="form-control" id="joined_year" name="joined_year"/>
                          @endif
                      </div>
                    </div>
                    <div class="row">
                      <div  class="form-group col-md-3">
                        <button type="submit" class="btn btn-primary"> Done </button>
                      </div>
                    </div>
                    </form>
                    <div class="row">
                        <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Teacher's Name</th>
                            <th>Class</th>
                            <th>Joined Year</th>
                            <th>Options</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($all_students as $key => $student)
                          <tr>
                            <td>{{$student['first_name']}}</td>
                            <td>{{$student['last_name']}}</td>
                            <td>{{$student['teacher']['name']}}</td>
                            <td>{{$student['class']['class']}}</td>
                            <td>{{$student['joined_year']}}</td>
                            <td> 
                                <a href="{{url('read-student/'.$student['id'].'')}}"><button class="btn btn-success" type="button">Edit</button></a>
                                <a href="{{url('delete-student/'.$student['id'].'')}}"><button class="btn btn-danger" type="button">Delete</button></a>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
