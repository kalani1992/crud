<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
	protected $table = 'students';

	public function teacher(){
	  return $this->belongsTo(Teacher::class);
	}

	public function class(){
	  return $this->belongsTo(Grade::class);
	}
}
