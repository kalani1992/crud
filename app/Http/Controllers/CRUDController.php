<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Teacher;
use App\Grade;

class CRUDController extends Controller
{
	 
    function __construct(Request $request){
        $this->middleware('auth');
        $this->request = $request;
    }
  
    function index(){
    	$all_data = $this->read_all();
		return view('home')
		->with('all_students',$all_data['all_students'])
		->with('teachers',$all_data['all_teachers'])
		->with('classes',$all_data['all_classes']);
    }

	function create(){
	    try { 
			$student = new Student();
			$student->first_name = $this->request->first_name;
			$student->last_name = $this->request->last_name;
			$student->teacher_id = $this->request->teacher;
			$student->class_id = $this->request->class;
			$student->joined_year = $this->request->joined_year;
			$student->save();
			return redirect()->back();
		} catch (Exception $e) {
			report($e);
		}
	}

	function read($id){
		$record_to_edit = Student::where('id',$id)->with('teacher')->with('class')->get()->toArray();
		$all_data = $this->read_all();
		return view('home')->with('all_students',$all_data['all_students'])->with('teachers',$all_data['all_teachers'])->with('classes',$all_data['all_classes'])->with('record_to_edit',$record_to_edit);
	}

	function update($id){
		try {
		$record_to_edit = Student::where('id',$id)->update(['first_name'=> $this->request->first_name,'last_name'=> $this->request->last_name,'teacher_id'=> $this->request->teacher,'class_id' => $this->request->class, 'joined_year' => $this->request->joined_year]);
		return redirect('home');
		} catch (Exception $e) {
		report($e);
		}
	}

	function delete($id){
	    try {
			Student::find($id)->delete();
			return redirect('home');	
		} catch (Exception $e) {
			report($e);
		}
	}

	function read_all(){
		$data = array();
		$data['all_students'] = Student::with('teacher')->with('class')->get()->toArray();
    	$data['all_teachers'] = Teacher::all()->toArray();
    	$data['all_classes'] = Grade::all()->toArray();
    	return $data;
	}
}
