<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'CRUDController@index')->name('home');

Route::post('/create-student', 'CRUDController@create')->name('create');

Route::get('/read-student/{student_id}', 'CRUDController@read')->name('read');

Route::post('/update-student/{student_id}', 'CRUDController@update')->name('update');

Route::any('/delete-student/{student_id}', 'CRUDController@delete')->name('delete');
